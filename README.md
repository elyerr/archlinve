# **Archlinux ISO**
  Esta configuracion crea una iso de arch a partir de una configuracion personalizada que incluye una Interfaz grafica de usuario con LXDE, para ello se usa archiso que propociona herramientas para la creacion de isos oficiales de arch.

### **Repositorio relend**
```
git clone https://gitlab.com/sanghas/archinstaller.git
```

### **Dependencias necesarias**
```
sudo pacman -S archiso
```

### **Usuario** 
- **root:** deshabilitado
- **Usuario:** admin
- **Password:** admin
- **SSH Port:** 22
- **Idioma:** es_PE.UTF-8
- **Teclado:** latinoamericano

### **Generar iso**
- **sudo mkarchiso:** Comando para la creacion de la iso
- **Descripcion de los parametros**
  - **-v :** Permite mostrar el proceso del desarrollo de la iso.
  - **-w /home/tu_usuario/pack :** lugar donde se descargara todo y se empaquetara usando la congiguracion del repositorio clonado, esta carpeta puede ser removidad despues de terminar la iso ya que no sera necesaria.
  - **-o /home/tu_usuario/iso :** lugar donde se guardará la iso cuando termine el proceso de empaquetado.
  - **repositorio_clonado :** Lugar del repositorio clonado en donde se guarda la configuracion para la creacion de la iso de archlinux
  
```
sudo mkarchiso -v -w /home/tu_usuario/pack -o /home/tu_usuario/iso repositorio_clonado
```
